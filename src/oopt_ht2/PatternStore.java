package oopt_ht2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PatternStore {

    private static PatternStore instance;
    private ArrayList<Pattern> patterns;

    public PatternStore() {
        this.patterns = new ArrayList();
    }

    public static PatternStore getInstance() {
        if (instance == null) {
            instance = new PatternStore();
        }
        return instance;
    }

    public void addPattern(Pattern p) {
        patterns.add(p);
    }

    public ArrayList<Pattern> getPatterns() {
        return patterns;
    }
    
    public ArrayList<String> getAllKeywords() {
        
        Set<String> list = new HashSet<>();
        
        for (Pattern p : patterns) {
            for (String s : p.getKeywords()) {
                list.add(s);
            }
        }
        
        ArrayList<String> keywords = new ArrayList();
        
        for (String s : list)
            keywords.add(s);
        
        return keywords;
    }
    
}
