package oopt_ht2;

import java.util.ArrayList;

public class Pattern {

    private String name;
    private String type;
    private ArrayList<String> keywords;
    private String filename;

    public Pattern(String name, String type, String filename) {
        this.name = name;
        this.type = type;
        this.filename = filename;
        this.keywords = new ArrayList();
        this.keywords.add(this.type);
    }

    public void addKeyword(String kw) {
        this.keywords.add(kw);
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public ArrayList<String> getKeywords() {
        return this.keywords;
    }

    public String getFilename() {
        return this.filename;
    }
    
    @Override
    public String toString() {
        return this.name + " pattern";
    }
    
}
